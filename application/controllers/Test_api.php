<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('guzzle');
	}

	function index()
	{
		$this->load->view('api_view');
	}

	function action()
	{
		if($this->input->post('data_action'))
		{
			# guzzle client define
			$client     = new GuzzleHttp\Client();
			$data_action = $this->input->post('data_action');

			if($data_action == "Delete")
			{
				$url = "http://localhost/test/api/delete";

				try {
				    # guzzle post request example with form parameter
				    $response = $client->request( 'POST', 
				                                   $url, 
				                                  [ 'form_params' 
				                                        => [ 
				                                        'id' => $this->input->post('user_id')
				                                        ] 
				                                  ]
				                                );
				    #guzzle repose for future use
				    //echo $response->getStatusCode(); // 200
				    //echo $response->getReasonPhrase(); // OK
				    //echo $response->getProtocolVersion(); // 1.1
				    echo $response->getBody();
				  } catch (GuzzleHttp\Exception\BadResponseException $e) {
						    #guzzle repose for future use
						    $response = $e->getResponse();
						    $responseBodyAsString = $response->getBody()->getContents();
						    print_r($responseBodyAsString);
				  }

			}

			if($data_action == "Edit")
			{
				$url = "http://localhost/test/api/update";
				
				try {
				    # guzzle post request example with form parameter
				    $response = $client->request( 'POST', 
				                                   $url, 
				                                  [ 'form_params' 
				                                        => [ 
				                                        'id' => $this->input->post('user_id'),
				                                        'first_name' => $this->input->post('first_name'),
				                                        'last_name' => $this->input->post('last_name') 
				                                        ] 
				                                  ]
				                                );
				    #guzzle repose for future use
				    //echo $response->getStatusCode(); // 200
				    //echo $response->getReasonPhrase(); // OK
				    //echo $response->getProtocolVersion(); // 1.1
				    echo $response->getBody();
				  } catch (GuzzleHttp\Exception\BadResponseException $e) {
						    #guzzle repose for future use
						    $response = $e->getResponse();
						    $responseBodyAsString = $response->getBody()->getContents();
						    print_r($responseBodyAsString);
				  }

			}

			if($data_action == "fetch_single")
			{
				$url = "http://localhost/test/api/fetch_single";

				try {
				    # guzzle post request example with form parameter
				    $response = $client->request( 'POST', 
				                                   $url, 
				                                  [ 'form_params' 
				                                        => [ 
				                                        'id' => $this->input->post('user_id')
				                                        ] 
				                                  ]
				                                );
				    #guzzle repose for future use
				    //echo $response->getStatusCode(); // 200
				    //echo $response->getReasonPhrase(); // OK
				    //echo $response->getProtocolVersion(); // 1.1
				    echo $response->getBody();
				  } catch (GuzzleHttp\Exception\BadResponseException $e) {
						    #guzzle repose for future use
						    $response = $e->getResponse();
						    $responseBodyAsString = $response->getBody()->getContents();
						    print_r($responseBodyAsString);
				  }

			}

			//test insert pakai guzzle
			if($data_action == "Insert")
			{	  
				  #This url define speific Target for guzzle
				  $url = "http://localhost/test/api/insert";

				  #guzzle
				  
				  try {
				    # guzzle post request example with form parameter
				    $response = $client->request( 'POST', 
				                                   $url, 
				                                  [ 'form_params' 
				                                        => [ 
				                                        'first_name' => $this->input->post('first_name'),
				                                        'last_name' => $this->input->post('last_name') 
				                                        ] 
				                                  ]
				                                );
				    #guzzle repose for future use
				    //echo $response->getStatusCode(); // 200
				    //echo $response->getReasonPhrase(); // OK
				    //echo $response->getProtocolVersion(); // 1.1
				    echo $response->getBody();
				  } catch (GuzzleHttp\Exception\BadResponseException $e) {
						    #guzzle repose for future use
						    $response = $e->getResponse();
						    $responseBodyAsString = $response->getBody()->getContents();
						    print_r($responseBodyAsString);
				  }


			}

			if($data_action == "fetch_all")
			{
				$api_url = "http://localhost/test/api";

				try {
				    # guzzle post request example with form parameter
				    $response = $client->request('PUT',$api_url,array());
				    #guzzle repose for future use
				    //echo $response->getStatusCode(); // 200
				    //echo $response->getReasonPhrase(); // OK
				    //echo $response->getProtocolVersion(); // 1.1
				    //echo $response->getBody();
				  } catch (GuzzleHttp\Exception\BadResponseException $e) {
						    #guzzle repose for future use
						    $response = $e->getResponse();
						    $responseBodyAsString = $response->getBody()->getContents();
						    print_r($responseBodyAsString);
				  }
				 
				$result = json_decode($response->getBody());

				$output = '';

				if(count($result) > 0)
				{
					foreach($result as $row)
					{
						$output .= '
						<tr>
							<td>'.$row->first_name.'</td>
							<td>'.$row->last_name.'</td>
							<td><butto type="button" name="edit" class="btn btn-warning btn-xs edit" id="'.$row->id.'">Edit</button></td>
							<td><button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row->id.'">Delete</button></td>
						</tr>

						';
					}
				}
				else
				{
					$output .= '
					<tr>
						<td colspan="4" align="center">No Data Found</td>
					</tr>
					';
				}

				echo $output;
			}
		}
	}
	
}

?>